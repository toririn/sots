namespace :channels do
  desc "SlackのChannel一覧を取得してDBへ挿入する"
  task update: :environment do |task|
    puts "Start Channel Update!"
    start_time = Time.zone.now

    teams  = Slacater::Team.authenticated
    next if teams.blank?

    create_channels = []
    teams.each do |team|
      client   = SimpleSlack::Client.new(team.decrypt_token)
      channels = (client.get.channels rescue nil)
      next if channels.blank?

      puts "Update Start #{team.name} channels"
      channels.each do |channel|
        slacater_channel = Slacater::Channel.find_or_initialize_by(team_id: team.id, slack_id: channel[:id])
        if slacater_channel.new_record?
          slacater_channel.name = channel[:name]
          create_channels << slacater_channel
        else
          slacater_channel.update(name: channel[:name])
        end
      end
    end

    Slacater::Channel.import create_channels

    end_time = Time.zone.now

    puts 'Channel update Success!'
    puts "New Channel Record: #{create_channels.size}"
    puts "Update Channel Record: #{Slacater::Channel.all.size - create_channels.size}"
    puts "Rake task time: #{end_time - start_time}\/s"
  end
end
