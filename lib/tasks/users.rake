namespace :users do
  desc "SlackのUser一覧を取得してDBへ挿入する"
  task update: :environment do |task|
    puts "Start User Update!"
    start_time = Time.zone.now

    teams  = Slacater::Team.authenticated
    next if teams.blank?

    create_users = []
    teams.each do |team|
      client = SimpleSlack::Client.new(team.decrypt_token)
      users  = (client.get.users rescue nil)
      next if users.blank?

      puts "Update Start #{team.name} users"
      users.each do |user|
        slacater_user = Slacater::User.find_or_initialize_by(team_id: team.id, slack_id: user[:id])
        if slacater_user.new_record?
          slacater_user.slack_name = user[:name]
          create_users << slacater_user
        else
          slacater_user.update(slack_name: user[:name])
        end
      end
    end

    Slacater::User.import create_users

    end_time = Time.zone.now

    puts 'User update Success!'
    puts "New User Record: #{create_users.size}"
    puts "Update User Record: #{Slacater::User.all.size - create_users.size}"
    puts "Rake task time: #{end_time - start_time}\/s"
  end
end
