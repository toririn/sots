namespace :teams do
  desc "Team#token からSlackのTeamを取得してDBへ挿入する"
  task update: :environment do
    teams = Slacater::Team.unauthenticated
    return puts 'no changes' && next if teams.blank?

    update_teams = []
    teams.each do |team|
      Slack.configure { |config| config.token = team.decrypt_token }
      client    = Slack.client
      team_info = (client.team_info rescue nil)

      next unless info = (team_info["team"].presence rescue nil)
      if team.update(slack_id: info["id"], name: info["name"], domain: info["domain"], token: team.decrypt_token)
        update_teams << team
      else
        next
      end
    end
    puts "Teams アップデート完了！"
    puts update_teams.join("\n")
  end
end
