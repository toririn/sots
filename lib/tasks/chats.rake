namespace :chats do
  desc "SlackのChannelごとの一日分のChatを取得してDBへ挿入する"
  task update: :environment do |task|
    puts "Start Chat Update!"
    start_time = Time.zone.now

    teams  = Slacater::Team.authenticated
    next if teams.blank?

    create_chats   = []
    error_messages = []
    teams.each do |team|
      users    = Slacater::User.where(team_id: team.id).pluck(:id, :slack_id)
      users_hash = users.map{|user| { user[1] => user[0] } }.flatten.uniq

      channels = Slacater::Channel.where(team_id: team.id)
      next if channels.blank?
      client   = SlacatSupport::Slacker.new(team.decrypt_token)

      puts "Update Start #{team.name} channels chats"
      channels.each do |channel|
        chats = (client.search(channel: channel.slack_id, latest: Time.zone.now.end_of_day.to_i, oldest: Time.zone.now.beginning_of_day.to_i, type: :message) rescue nil)
        next if chats.blank?
        chats.each do |chat|
          user_id = users_hash.find{ |user_hash| user_hash[chat[:user_id]] }.values.first
          create_chat = Slacater::Chat.new(team_id: team.id, channel_id: channel.id, user_id: user_id, text: chat[:text], posted_at: chat[:posted_at])
          if create_chat.valid?
            create_chats << create_chat
          else
            error_messages << create_chat.errors.full_messages.join("\n")
          end
        end
      end
    end

    Slacater::Chat.import create_chats

    end_time = Time.zone.now

    puts 'Chat update Success!'
    puts "New Chat Record: #{create_chats.size}"
    puts "Rake task time: #{end_time - start_time}\/s"
  end
end
