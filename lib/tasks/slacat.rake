namespace :slacat do
  desc "SlackのUser一覧を取得してDBへ挿入する"
  task update: :environment do |task|
    puts "Start Slacat Update!"
    start_time = Time.zone.now

    `bundle exec rake teams:update`
    `bundle exec rake channels:update`
    `bundle exec rake users:update`

    end_time = Time.zone.now

    puts "Complate!"
    puts "Task times: #{end_time - start_time}/s"
  end
end
