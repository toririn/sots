Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :teams, only: [] do
        resources :users, only: [:index] do
          resources :chats, only: [:create]

          namespace :chats do
            resources :search_chats, only: [:index]
          end
        end
      end
    end
  end
end
