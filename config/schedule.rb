set :output, "./log/whenever.log"
set :environment, :development
env :PATH, ENV['PATH']

every '0,2,30,45 * * * *' do
  rake 'teams:update'
end
